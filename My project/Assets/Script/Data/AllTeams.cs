using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AllTeams", menuName = "Data/AllTeams", order = 1)]
public class AllTeams : ScriptableObject
{
   public List<Team> teams;
}
[System.Serializable]   
public class Team
{
    public string name;
    public int index;
    public TeamsData teams;

}


