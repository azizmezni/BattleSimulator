using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitData", menuName = "Data/UnitData", order = 1)]
public class UnitData : ScriptableObject
{
  
    public float Health;

    public float Attack;
    
    public float AttackSpeed;
 
    public float AttackRange;
    
    public float MovementSpeed;
}
