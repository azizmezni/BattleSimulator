using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "TeamsData", menuName = "Data/TeamsData", order = 1)]
public class TeamsData : ScriptableObject
{
   public List<UnitData> unitDatas = new List<UnitData>();
    public GameObject prefabRef;
}
