using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    public AllTeams allTeams;
    public Button buttonRef;
    public Transform Parrent;
    void Start()
    {
        foreach (var team in allTeams.teams)
        {

          var btn=  Instantiate(buttonRef, Parrent);
            btn.gameObject.SetActive(true);
            btn.onClick.AddListener(()=>OnSelectTeam(team));
            btn.GetComponentInChildren<TextMeshProUGUI>().text = team.name;
        }
    }

   void OnSelectTeam(Team team)
    {
        SetupManager.instance.SelectTeam(team);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
