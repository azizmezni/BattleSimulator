using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI.Table;

public class SetupManager : MonoBehaviour
{
    public static SetupManager instance;
    public AllTeams Allteams;
    public Team playerTeam;
    public Team AiTeam;
    public List<GameObject> spawnedAi= new List<GameObject>();
    public List<GameObject> spawnedPlayer = new List<GameObject>();
    public Transform PlayerSpawnPos;
    public Transform AiSpawnPos;
    public bool GamehasStarted;
    public static Action<bool> GameEnd;
    public void restartGame()
    {
        spawnTeam(playerTeam, PlayerSpawnPos, spawnedPlayer, false);
        for (int i = spawnedAi.Count - 1; i >= 0; i--)
        {
            Destroy(spawnedAi[i]);

        }
        spawnedAi.Clear();
    }
    public void startGame()
    {
        GamehasStarted = true;
    }
    public void EndGame(bool isAi)
    {
        GamehasStarted = false;

    }
    private void Awake()
    {
        instance = this;
    }
    public void SelectTeam(Team team)
    {
        AiTeam = team;
        spawnTeam(AiTeam, AiSpawnPos, spawnedAi);
    }

    private void spawnTeam(Team team, Transform location, List<GameObject> list, bool isAi=true)
    {
       for (int i = list.Count-1; i >=0; i--)
        {
            Destroy(list[i]);

        }
        list.Clear();
        int row = 0;
        int col = 0;

        for (int i = 0; i < team.teams.unitDatas.Count; i++)
        {
            // Calculate position for each cube in the grid
            Vector3 spawnPos = new Vector3((col+ location.position.x)*2, 0.5f, (row + location.position.y)*2 );
            GameObject AIprefab = Instantiate(team.teams.prefabRef, spawnPos, Quaternion.identity);
            AIprefab.GetComponent<playerData>().setPlayerData(team.teams.unitDatas[i],isAi);
            list.Add(AIprefab);

         col++;
            if (col >= 3)
            {
                col = 0;
                row++;
            }
        }
    }

    void Start()
    {
        GameEnd += EndGame;
        spawnTeam(playerTeam,PlayerSpawnPos,spawnedPlayer, false);
    }

   

    // Update is called once per frame
    void Update()
    {
        
    }
}
