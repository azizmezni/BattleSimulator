using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGameUI : MonoBehaviour
{
    public GameObject pannel;
    public TextMeshProUGUI Txt;
    void Start()
    {
        SetupManager.GameEnd += OnGameEnd;   
    }
    private void OnDestroy()
    {
        SetupManager.GameEnd -= OnGameEnd;
    }
    private void OnGameEnd(bool obj)
    {
        pannel.SetActive(true);
        Txt.text = obj ? "Ai " : "You ";
        Txt.text += "Win";
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
