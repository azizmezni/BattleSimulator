using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class playerData : MonoBehaviour
{
   private UnitData unitData;
    float currentHealth;
  [SerializeField]  private TextMeshPro healthUI;
    NavMeshAgent agent;
    Transform target;
    bool IsAI;
    float timer;
    public void setPlayerData(UnitData unitData, bool isAi)

    { this.unitData = unitData;
        currentHealth = unitData.Health;
        healthUI.text = currentHealth.ToString();
        if(agent== null)
            agent=GetComponent<NavMeshAgent>();
        agent.speed = unitData.MovementSpeed;
        agent.stoppingDistance = unitData.AttackRange;
        IsAI= isAi;

    }
    void takeDamage(float damage)
    {
        currentHealth-=damage;
        if (currentHealth <= 0)
            Destroy(gameObject);
        healthUI.text=currentHealth.ToString();


    }

    void Start()
    {

    }

   

    // Update is called once per frame
    void Update()
    {
        if (!SetupManager.instance.GamehasStarted)
            return;
        Movement();
        AttackHandler();

    }

    private void AttackHandler()
    {
        timer += Time.deltaTime;
        if(timer>unitData.AttackSpeed)
        {
            timer = 0;
            DoAttack();
        }
    }

    private void DoAttack()
    {
        if(target!=null)
        {
            target.GetComponent<playerData>().takeDamage(unitData.Attack);
        }
    }

    private void Movement()
    {
        if (target == null)
        {
            var list = IsAI ? SetupManager.instance.spawnedPlayer.FindAll(x => x != null) : SetupManager.instance.spawnedAi.FindAll(x => x != null);

            if (list == null || list.Count == 0)
            {
                SetupManager.GameEnd?.Invoke(IsAI);
               // Debug.LogError("Empty list team win");
                return;
            }

            target = list[Random.Range(0, list.Count)].transform;
          


        }
        else
        {
            agent.SetDestination(target.position);
        }
    }
}
